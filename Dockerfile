FROM tensorflow/tensorflow:1.14.0-py3

WORKDIR /usr/src/app
COPY . .

RUN apt-get update && apt-get install libglib2.0-0 libxext6 libsm6 libxrender1 libpq-dev -y
RUN pip install --upgrade pip
RUN pip install -r requirements.txt
RUN pip install https://github.com/OlafenwaMoses/ImageAI/releases/download/2.0.3/imageai-2.0.3-py3-none-any.whl

CMD python main.py