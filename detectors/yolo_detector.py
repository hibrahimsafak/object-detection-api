import os
from imageai.Detection import ObjectDetection


class YOLOv3Detector:
    def __init__(self):
        self.detector = ObjectDetection()
        self.only_person = self.detector.CustomObjects(person=True)

    def load_model(self):
        execution_path = os.getcwd()
        self.detector.setModelTypeAsYOLOv3()
        self.detector.setModelPath(os.path.join(execution_path, 'models/yolo.h5'))
        self.detector.loadModel(os.environ.get('MODEL_SPEED', 'normal'))

    def detect_object(self, image):
        """
        :param image:
        :return:
        """
        objects = []
        detected_copy, output_objects = self.detector.detectCustomObjectsFromImage(
            input_image=image,
            input_type="array",
            output_type="array",
            extract_detected_objects=False,
            minimum_percentage_probability=50
        )

        if len(output_objects) == 0:
            return None

        for output_object in output_objects:
            left, top, right, bottom = output_object['box_points']
            objects.append({
                'name': output_object['name'],
                'accuracy': output_object['percentage_probability'],
                'x': int(left),
                'y': int(top),
                'width': (int(right) - int(left)),
                'height': (int(bottom) - int(top))
            })

        return objects
