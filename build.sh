#!/bin/bash
outer_port=10009
inner_port=8080

docker rm -f object-detection-api
docker build -t object-detection-api .
docker run -d --name object-detection-api -p ${outer_port}:${inner_port} -e PORT=${inner_port} object-detection-api
