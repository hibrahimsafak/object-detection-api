import flask
import cv2
import numpy as np
import json
import base64
import time
import os

from detectors import YOLOv3Detector

app = flask.Flask(__name__)
detector = YOLOv3Detector()


@app.route("/", methods=["GET"])
def health():
    response = flask.jsonify(status='ok', time=time.time())
    response.status_code = 200
    return response


@app.route("/api/object-detection", methods=["POST"])
def detection():
    data = {'success': False}

    req = flask.request
    body = json.loads(req.data)
    encoded_image = body['image']

    np_array = np.fromstring(base64.b64decode(encoded_image), np.uint8)
    image = cv2.imdecode(np_array, cv2.IMREAD_COLOR)
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

    detector_response = detector.detect_object(image)
    if detector_response is not None:
        data['success'] = True
        data['predictions'] = detector_response
    else:
        data['predictions'] = []

    response = flask.jsonify(success=data['success'], predictions=data['predictions'])
    response.status_code = 200
    return response


if __name__ == "__main__":
    detector.load_model()
    port = os.environ.get('PORT', 8080)
    app.run(threaded=False, host='0.0.0.0', port=port)
